import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;



/* Hurkle Game in Java 
 * Version 2
 * Added functionlity so user can select where in the grid he or she thinks the hurkle is hiding. 
 * 
 */

public class Hurkle extends JFrame{
	
	final private static int ROW_SIZE = 10;
	final private static int COL_SIZE = 10;
	private static JMenuBar menuBar;
	private static int[][] grid;
	private static JLabel statusLabel;
	private static JButton answer = new JButton("Submit");	
	private static JTextField row = new JTextField(2);
	private static JTextField col = new JTextField(2);
	private static int hrow;
	private static int hcol;	
	
	private static JMenuBar createMenu()
	{
		
		menuBar = new JMenuBar();
		
		//Build game menu
		JMenu game = new JMenu("Game");
		game.setMnemonic(KeyEvent.VK_G);
		game.getAccessibleContext().setAccessibleDescription("This menu controls the game");
		
		
		JMenuItem newGame = new JMenuItem("New Game");
		JMenuItem exit = new JMenuItem("Exit");
		newGame.setMnemonic(KeyEvent.VK_N);
		exit.setMnemonic(KeyEvent.VK_X);
		
		game.add(newGame);
		game.add(exit);
		
		JMenu help = new JMenu("Help");
		help.setMnemonic(KeyEvent.VK_H);
		
		
		JMenuItem viewHelp = new JMenuItem("View Help");
		JMenuItem about= new JMenuItem("About Hurkle");
		help.add(viewHelp);
		help.add(about);
		
		menuBar.add(game);
		menuBar.add(help);

		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0); // Exit the program
			}
		});
		
		newGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				
			}
		});
		
		
		viewHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				
			}
		});
		
		about.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				
			}
		});
		
		return menuBar;
	}

	
	public static void main(String[] args) {
		
		JFrame frame = new Hurkle();
		frame.setSize(800,600);
		frame.setTitle("Hurkle");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setJMenuBar(createMenu());
	}
	
	
	public Hurkle() {
		
		init();
		setLayout(new BorderLayout());
		add(getContent(), BorderLayout.CENTER);
		
		statusLabel = new JLabel();
		
		
		JPanel status = new JPanel();
		
		
		status.add(statusLabel);
		
		
		add(status, BorderLayout.NORTH);
		
	
		JPanel inputPanel = new JPanel();
		
		JLabel questionText = new JLabel("The hurkle is hiding. Can you guess where he is?");
		JLabel rowText = new JLabel("Row:");
		JLabel colText = new JLabel("Col:");
		
		questionText.setAlignmentX(LEFT_ALIGNMENT);
		

		
		inputPanel.add(questionText);
		inputPanel.add(rowText);
		inputPanel.add(row);
		inputPanel.add(colText);
		inputPanel.add(col);
		inputPanel.add(answer);
		
		
		add(inputPanel, BorderLayout.SOUTH);
	}
	
	
	private static void setStatus(String status )
	{	
		statusLabel.setText(status);	
	}
	
	
	private static JPanel drawGrid()
	{
		
		JPanel gridPanel = new JPanel();
		gridPanel.setLayout(new GridLayout(ROW_SIZE+1, COL_SIZE+1,5,0));
		
		gridPanel.add(new JLabel(" "));
		
		/*  Display header row */
		for (Integer i=0; i<COL_SIZE; i++)
		{
			gridPanel.add(new JLabel(i.toString()));
		}
			
		for (Integer i=0; i<ROW_SIZE; i++)
		{
			gridPanel.add(new JLabel(i.toString()));	
			for (int j=0; j<COL_SIZE; j++)
			{
				  gridPanel.add(new JLabel("*")); 
			}
			
		}
		
		return gridPanel;
	}
	
	private static JPanel getContent()
	{
		// Set up gridPanel
		
		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());

		// Grid Labels
		
		JLabel north = new JLabel("North", JLabel.CENTER);
		JLabel south = new JLabel("South", JLabel.CENTER);
		JLabel east = new JLabel("East", JLabel.CENTER);
		JLabel west = new JLabel("West", JLabel.CENTER);
		
		north.setForeground(Color.BLUE);
		south.setForeground(Color.BLUE);
		east.setForeground(Color.BLUE);
		west.setForeground(Color.BLUE);
		
		contentPane.add(north, BorderLayout.NORTH);
		contentPane.add(south, BorderLayout.SOUTH);
		contentPane.add(east, BorderLayout.EAST);
		contentPane.add(west, BorderLayout.WEST);
		
		
		contentPane.add(drawGrid(), BorderLayout.CENTER);
		
		return contentPane;	
	}
	
	
	
	
	private static void init()
	{	
			// Initalize Grid
			grid = new int[COL_SIZE][ROW_SIZE];
			
			for (int i =0; i<COL_SIZE; i++)
			{
				for (int j=0; j<ROW_SIZE; j++)
				{
					grid[i][j]=0;
				}
			}
			
			
			addButtonListener();
			

			//Hide hurkle
			 hcol = (int)( Math.random()*ROW_SIZE)+1;
			 hrow = (int) (Math.random()*COL_SIZE)+1;
			 
			 
	}
	
	
	private static void addButtonListener()
	{
        //Add action listener to button
        answer.addActionListener(new ActionListener() {
 
            public void actionPerformed(ActionEvent e)
            {
               
            	StringBuilder temp = new StringBuilder();
            	
            	try
            	{

            		
                	int guessRow = Integer.parseInt(row.getText());
                	int guessCol = Integer.parseInt(col.getText());
            
            	
            		if (guessRow==hrow && guessCol==hcol)
            		{
            			setStatus("You win!!!!!");
            			System.exit(0);
            		}
            	
            		else	
            		{
            		temp.append("Sorry, I am not hiding at location " + row.getText() + "," + col.getText() +". Try going ");
            		
            		if (guessRow>hrow)
            		{
            			temp.append("North ");
            		}
            		
            		if (guessRow<hrow)
            		{
            			temp.append("South ");
            		}
            		
            		if (guessCol>hcol)
            		{
            			temp.append("West ");
            		}
            		
            		if (guessCol<hcol)
            		{
            			temp.append("East ");
            		}
            	}
            	
            	setStatus(temp.toString());
            	}
                
            	catch (NumberFormatException e1)
            	{
            		setStatus("Invalid location. Try again.");
            	}
            	
            	row.setText(null);
            	col.setText(null); 
            }
        });      
 
	}
}
